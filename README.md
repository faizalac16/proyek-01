# proyek-01


pada proyek ini saya membuat sebuah CRUD yang mana mampu melihat nama, foto dan skill seorang programmer.
bahasa yang saya gunakan adalah PHP.



namun terdapat kendala ketika saya update. saya baru bisa menambahkan foto, skill, nama, dan user ID.
selain itu saya sudah bisa menjalankan sistem Update dan delete nya.

soal javascript saya kerjakan yang nomor 3, karena waktu saya berkutat di CRUD terlalu lama.

untuk konfigurasi filing nya, Folder config merupakan folder yang harus diperhatikan dalam menjalankan program saya. 
dimana $password tentunya disesuaikan dengan milik reviewer, dimana milik saya passwordnya tertera di dalam file.

nama database yang saya gunakan adalah hero.

file resources digunakan untuk menyimpan foto-foto yang di upload.
routes sebagai routing terminal atau penghubung antar file.

view tentu saja merupakan form input, read delete dan update data.

functions berisi beberapa file dengan ekstentsi PHP yang mana berisi fungsi2 yang membuat halaman di folder view bekerja secara dinamis.

soal nomor 1 dan 2 belum sempat saya selesaikan karena keterbatasan waktu, apabila saya gagal, saya akan melakukan yang terbaik.


