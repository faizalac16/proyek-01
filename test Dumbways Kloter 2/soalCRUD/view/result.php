<?php

require_once('../routes/route.php');


?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <div class="container py-5 justify-content-center w-75">
        <h1 style="text-align:center" class="mb-5">Form Lihat Data</h1>
        <a href="input.php"> <button class="btn btn-primary">  Tambahkan Data </button> </a>
        <?php read_data() ?>
    </div>
    
</body>
</html>