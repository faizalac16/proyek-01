<?php

    require_once('../routes/route.php');
?>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Input Form</title>
</head>

<div class="container py-5 w-50">
    <!-- Judul Halaman-->
    <h1 style="text-align:center;" class="py-5">Form Input Programmer</h1>

    <!-- Form inputan -->
    <form class="align-items-center " method="post" action="../functions/upload.php" enctype="multipart/form-data">

    <div class="form-group">
        <label for="exampleFormControlInput1">Nama Programmer</label>
        <input type="text" class="form-control" id="exampleFormControlInput1" name="name" placeholder="Trent Alexander-Arnold">
    </div>

    <div class="form-group">
        <label for="exampleFormControlSelect1">Skill</label>
        <input type="text" class="form-control" id="exampleFormControlInput1" name="skill" placeholder="React Native">
    </div>

    <div class="form-group">
        <label for="exampleFormControlTextarea1">Upload Foto</label>
        <input class="form-control" style="border:none;" name="gambar" type="file">
    </div>

    <input type="submit" value="Submit" name="submit" class="btn btn-primary">

    </form>

    

</div>